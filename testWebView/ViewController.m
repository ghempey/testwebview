//
//  ViewController.m
//  testWebView
//
//  Created by Grant Empey on 10/20/15.
//  Copyright © 2015 ExampleCompany. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self loadAboutHTML];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)loadAboutHTML {
    _backGroundWeb = [[UIWebView alloc] init];
    _backGroundWeb.delegate = self;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"html"]];
    [_backGroundWeb loadRequest:urlRequest];
    NSLog(@"view did done");
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"%@", [webView stringByEvaluatingJavaScriptFromString:@"remoteCall();"]);
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog([request.URL scheme]);
    
    switch (navigationType) {
            
        case UIWebViewNavigationTypeOther:
            if ([[request.URL scheme] isEqualToString:@"alert"]) {
                NSString *message = [request.URL host];
                NSLog(message);
                NSLog(@"%@", [webView stringByEvaluatingJavaScriptFromString:@"getData();"]);

                if (true) {
                    // the alert should be shown
                } else {
                    // don't show the alert
                    // just do nothing
                }
                
                return NO;
            }
            break;
        default: ;
            //ignore
    }
    return YES;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}
@end
