//
//  ViewController.h
//  testWebView
//
//  Created by Grant Empey on 10/20/15.
//  Copyright © 2015 ExampleCompany. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *backGroundWeb;
@end

